import React from "react";

import { HashRouter as Router, Switch, Route } from "react-router-dom";
import FileSend from "./components/FileSend/FilesSend";
import MainPopup from "./components/MainPopup/MainPopup";

const RouterComponent = () => {
    return (
        <Router>
            <Switch>
                <Route path="/file-send">
                    <FileSend />
                </Route>
                <Route path="/">
                    <MainPopup />
                </Route>
            </Switch>
        </Router>
    );
};

export default RouterComponent;
