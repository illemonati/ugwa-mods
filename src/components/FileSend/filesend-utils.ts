export interface FileMeta {
    filename: string;
    filesize: number;
}

export class FileSender {
    chuckSize: number = 50;
    ws: WebSocket;
    file: File;
    fileMeta: FileMeta;
    chunks: number = 0;
    fileContentB64: string = "";
    constructor(wsUrl: string, file: File) {
        this.ws = new WebSocket(wsUrl);
        this.file = file;
        this.fileMeta = {
            filename: this.file.name,
            filesize: this.file.size,
        };
    }

    async initialize() {
        await new Promise((r) => {
            this.ws.onopen = r;
        });
        this.chunks = this.calculateChunks();
        this.sendIdentityMessage();
        this.sendMeta();
        this.fileContentB64 = await this.getFileContentB64();
    }

    sendFile() {
        console.log(this.fileContentB64);
        console.log(this.fileContentB64.substring(0, 50));
        console.log(this.chunks);
        for (let i = 0; i < this.chunks; i++) {
            const chuck = this.fileContentB64.substring(
                i * this.chuckSize,
                (i + 1) * this.chuckSize
            );
            console.log(chuck);
            this.sendChunk(chuck);
        }
    }

    async getFileContentB64() {
        const reader = new FileReader();
        reader.readAsArrayBuffer(this.file);
        await new Promise((r) => (reader.onload = r));
        const content = btoa(
            Array.from(new Uint8Array(reader.result as ArrayBuffer))
                .map((b) => String.fromCharCode(b))
                .join("")
        );
        console.log(content);
        return content;
    }

    calculateChunks() {
        const b64length = ((4 * this.file.size) / 3 + 3) & ~3;
        return Math.ceil(b64length / this.chuckSize);
    }
    sendIdentityMessage() {
        const identityMessage = {
            type: "identify",
            id: "FILE-SENDER",
            name: `${Math.random() * 10}`,
        };
        this.ws.send(JSON.stringify(identityMessage));
    }
    sendMeta() {
        const message = {
            type: "message",
            message: JSON.stringify({
                fileMeta: this.fileMeta,
                chucks: this.chunks,
            }),
        };
        this.ws.send(JSON.stringify(message));
    }
    sendChunk(chuckStr: string) {
        const message = {
            type: "message",
            message: chuckStr,
        };
        this.ws.send(JSON.stringify(message));
    }
}
