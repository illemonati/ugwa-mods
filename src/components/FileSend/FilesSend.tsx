import React, { createRef } from "react";
import { wsUrl } from "../../configs.json";

import { Button } from "@material-ui/core";

import "./styles.css";
import { FileSender } from "./filesend-utils";

const FileSend = () => {
    const fileInputRef = createRef<HTMLInputElement>();

    const sendFile = async () => {
        if (!fileInputRef.current) return;
        if (!fileInputRef.current.files) return;
        const file = fileInputRef.current.files[0];

        const fileSender = new FileSender(wsUrl, file);
        await fileSender.initialize();
        fileSender.sendFile();
    };

    return (
        <div className="FileSend">
            <Button component="label">
                Choose File
                <input type="file" hidden ref={fileInputRef} />
            </Button>
            <Button onClick={sendFile}>Send</Button>
        </div>
    );
};

export default FileSend;
