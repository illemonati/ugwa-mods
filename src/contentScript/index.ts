// If your extension doesn't need a content script, just leave this file empty

// This is an example of a script that will run on every page. This can alter pages
// Don't forget to change `matches` in manifest.json if you want to only change specific webpages
// This needs to be an export due to typescript implementation limitation of needing '--isolatedModules' tsconfig

export async function studentSendFile() {
    const chatter = document.getElementById("chatter");
    console.log(chatter);
    const fileSendUi = document.createElement("iframe");
    // @ts-ignore
    fileSendUi.src = chrome.runtime.getURL("popup.html") + "#/file-send";
    fileSendUi.style.border = "0px";
    fileSendUi.style.height = "50px";
    fileSendUi.style.width = "100%";
    fileSendUi.classList.add("send-msg-wrapper");
    chatter?.insertBefore(fileSendUi, chatter?.lastChild);
    // console.log(browser.extension.getURL("popup.html"));
}

studentSendFile();
